import math

wiederholen = True

while wiederholen:

    inputNetz = input("Bitte geben Sie das Basisnetz in Dezimalschreibweise an: ")
    anzSubnetze = int(input("Bitte geben Sie die gewünschte Anzahl von Subnetzen ein: "))

    netzIP = inputNetz.split(".")
    # Check für letztes Oktett hier möglich

    # Berechnung der nächsten 2er-Potenz
    logAnzSubnetz = math.log2(anzSubnetze)
    if logAnzSubnetz % 1 == 0:
        logAnzSubnetz = int(logAnzSubnetz)
    else:
        logAnzSubnetz = int(logAnzSubnetz)+1
    hostAnzSubnetz = int(math.pow(2,(8-logAnzSubnetz)))

    if (hostAnzSubnetz > 2):

        # Berechnung des letzten SNM-Oktetts
        snmOktett = 0
        for i in range(0,logAnzSubnetz):
            snmOktett += math.pow(2,7-i)

        print("Anzahl der möglichen Hosts pro Netz: " + str(hostAnzSubnetz-2))
        print("Subnetzmaske: 255.255.255." + str(int(snmOktett)))

        for i in range(0,anzSubnetze):
            lastOkt = int(netzIP[3]) + (i * hostAnzSubnetz)
            print(str(i) + ". Netz: " + netzIP[0] + "." + netzIP[1] + "." + netzIP[2] + "." + str(lastOkt))

        print()
    else:
        print("Anzahl der Netze zu groß für den IP-Bereich.")
    eingabe = input("Erneut berechnent (Y/N)? ")

    if eingabe == "Y":
        print()
    else:
        wiederholen = False
